package ru.job4j.arrays;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Array flip.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 02.01.2019
 */
public class TurnTest {

    @Test
    public void whenTurnArrayWithEvenAmountOfElementsThenTurnedArray() {
        Turn turner = new Turn();
        int[] input = new int[]{4, 1, 6, 2};
        int[] result = turner.back(input);
        int[] expect = new int[]{2, 6, 1, 4};
        assertThat(result, is(expect));
    }

    @Test
    public void whenTurnArrayWithOddAmountOfElementsThenTurnedArray() {
        //напишите здесь тест, проверяющий переворот массива с нечётным числом элементов, например {1, 2, 3, 4, 5}.
        Turn turn = new Turn();
        int[] array = new int[]{5, 4, 3, 2, 1};
        int[] arrayBeforeTurn = turn.back(array);
        int[] expectedArrayAfterTurn = new int[]{1, 2, 3, 4, 5};
        assertThat(arrayBeforeTurn, is(expectedArrayAfterTurn));
    }
}
