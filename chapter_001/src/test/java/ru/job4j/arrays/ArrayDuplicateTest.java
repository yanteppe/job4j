package ru.job4j.arrays;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Test task Arrays: 6.8. Remove duplicates in an array.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 17.01.2019
 */
public class ArrayDuplicateTest {

    @Test
    public void whenRemoveDuplicatesThenArrayWithoutDuplicate() {
        String[] actualArray = {"Привет", "Мир", "Привет", "Супер", "Мир"};
        String[] expectedArray = {"Привет", "Мир", "Супер"};
        ArrayDuplicate arrayDuplicate = new ArrayDuplicate();
        String[] result = arrayDuplicate.remove(actualArray);
        assertThat(result, is(expectedArray));
    }
}
