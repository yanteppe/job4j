package ru.job4j.arrays;

import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Test task Arrays: 6.3. The array is filled with true or false
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 04.01.2019
 */
public class CheckTest {

    @Test
    public void whenDataNotMonoByTrueThenFalse() {
        Check check = new Check();
        boolean[] input = new boolean[] {true, false, true};
        boolean result = check.monoSolution1(input);
        assertThat(result, is(false));
    }

    @Test
    public void whenDataMonoByTrueThenTrue() {
        Check check = new Check();
        boolean[] input = new boolean[] {true, true, true};
        boolean result = check.monoSolution1(input);
        assertThat(result, is(true));
    }
}
