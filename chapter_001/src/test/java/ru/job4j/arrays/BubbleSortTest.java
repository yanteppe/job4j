package ru.job4j.arrays;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Test task Arrays: 6.5. Rearrange array sorting
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 12.01.2019
 */
public class BubbleSortTest {

    @Test
    public void whenSortArrayWithTenElementsThenSortedArray() {
        BubbleSort bubbleSort = new BubbleSort();
        int[] expectedArray = {0, 1, 1, 2, 3, 4, 5, 5, 7, 8};
        int[] sortedArray = bubbleSort.sort(new int[]{1, 5, 4, 2, 3, 1, 7, 8, 0, 5});
        assertThat(sortedArray, is(expectedArray));
    }
}