package ru.job4j.arrays;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class SquareTest {
    @Test
    public void whenBound3Then149() {
        int bound = 3;
        Square square = new Square();
        int[] actualArray = square.calculate(bound);
        int[] expectedArray = new int[]{1, 4, 9};
        assertThat(actualArray, is(expectedArray));
    }
}