package ru.job4j.arrays;

import org.junit.Test;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class FindLoopTest {

    @Test
    public void whenArrayHasLengh5Then0() {
        FindLoop findLoop = new FindLoop();
        int[] input = new int[] {5, 10, 3};
        int value = 3;
        int result = findLoop.indexOf(input, value);
        int expectedIndex = 2;
        assertThat(result, is(expectedIndex));
    }

    @Test
    public void resultShouldNotBeEqualToArrayIndex() {
        FindLoop findLoop = new FindLoop();
        int[] input = new int[] {5, 10, 3};
        int value = 1;
        int result = findLoop.indexOf(input, value);
        int expectedIndex = -1;
        assertThat(result, is(expectedIndex));
    }

}
