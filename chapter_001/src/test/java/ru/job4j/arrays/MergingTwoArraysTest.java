package ru.job4j.arrays;

import org.junit.Assert;
import org.junit.Test;

/**
 * Test task - Merging two arrays and sort.
 *
 * @author Yan Teppe
 * @since 17.02.2019
 */
public class MergingTwoArraysTest {

    @Test
    public void twoSortedArraysShouldBeMergedAndSorted() {
        int[] arrayA = {0, 5, 9, 8, 10};
        int[] arrayB = {3, 6, 7, 11, 16};
        int[] expectedArray = {0, 3, 5, 6, 7, 8, 9, 10, 11, 16};
        MergingTwoArrays mergingTwoArrays = new MergingTwoArrays();
        int[] actualArray = mergingTwoArrays.mergingTwoArraysAndSort(arrayA, arrayB);
        Assert.assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void twoUnsortedArraysShouldBeMergedAndSorted() {
        int[] arrayA = {5, 0, 10, 8, 9};
        int[] arrayB = {16, 11, 7, 6, 3};
        int[] expectedArray = {0, 3, 5, 6, 7, 8, 9, 10, 11, 16};
        MergingTwoArrays mergingTwoArrays = new MergingTwoArrays();
        int[] actualArray = mergingTwoArrays.mergingTwoArraysAndSort(arrayA, arrayB);
        Assert.assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void twoDifferentArraysShouldBeMergedAndSorted() {
        int[] arrayA = {0, 9};
        int[] arrayB = {16, 7, 5, 11, 3};
        int[] expectedArray = {0, 3, 5, 7, 9, 11, 16};
        MergingTwoArrays mergingTwoArrays = new MergingTwoArrays();
        int[] actualArray = mergingTwoArrays.mergingTwoArraysAndSort(arrayA, arrayB);
        Assert.assertArrayEquals(expectedArray, actualArray);
    }

    @Test
    public void twoSortedArraysWithNegativeNumberShouldBeMergedAndSorted() {
        int[] arrayA = {9, 5, 0, 10, 8};
        int[] arrayB = {3, 6, -16, 11, 7};
        int[] expectedArray = {-16, 0, 3, 5, 6, 7, 8, 9, 10, 11};
        MergingTwoArrays mergingTwoArrays = new MergingTwoArrays();
        int[] actualArray = mergingTwoArrays.mergingTwoArraysAndSort(arrayA, arrayB);
        Assert.assertArrayEquals(expectedArray, actualArray);
    }
}
