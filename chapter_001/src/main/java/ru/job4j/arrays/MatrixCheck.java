package ru.job4j.arrays;

/**
 * Task Arrays: 6.7. Square array is filled with true or false diagonals.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 16.01.2019
 */
public class MatrixCheck {
    public boolean mono(boolean[][] data) {
        boolean result = true;

        // The first solution
        for (int i = 1; i < data.length; i++) {
            if (data[0][0] != data[i][i]) {
                result = false;
                break;
            }
            if (data[0][data.length - 1] != data[i][data.length - 1 - i])
                result = false;
            break;
        }
        return result;
    }


/*      The second solution
        for (int i = 0; i < data.length - 1; i++) {
            if (data[i][i] != data[i + 1][i + 1]) {
                result = false;
            }
        }

        for (int i = data.length - 1; i < 0; i--) {
            if (data[i][i] != data[i - 1][i - 1]) {
                result = false;
            }
*/

}