package ru.job4j.arrays;

// заполнить массив через цикл элементами от 1 до bound возведенными в квадрат
public class Square {
    public int[] calculate(int bound) {
        int[] array = new int[bound];
        array[0] = 1;
        for (int i = 1; i < bound; i++) {
            array[i] = (int) Math.pow(i + 1, 2);
        }
        return array;
    }
}
