package ru.job4j.arrays;

/**
 * Task. Merging two arrays and sort.
 *
 * @author Yan Teppe
 * @since 17.02.2019
 */
public class MergingTwoArrays {

    public int[] mergingTwoArraysAndSort(int arrayA[], int arrayB[]) {
        int[] resultArray = new int[arrayA.length + arrayB.length];
        int indexA = 0;
        // Merging two arrays into the result array
        for (int i = 0; i < arrayA.length; i++) {
            resultArray[i] = arrayA[i];
            indexA = i;
        }
        int indexB = 0;
        for (int i = indexA + 1; i < resultArray.length; i++) {
            resultArray[i] = arrayB[indexB++];
        }
        // Sorting result array
        for (int i = resultArray.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (resultArray[j] > resultArray[j + 1]) {
                    int element = resultArray[j];
                    resultArray[j] = resultArray[j + 1];
                    resultArray[j + 1] = element;
                }
            }
        }
        return resultArray;
    }
}
