package ru.job4j.arrays;

/**
 * Task Arrays: 6.6. Two-dimensional array. Multiplication table.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 15.01.2019
 */
public class Matrix {
    public int[][] multiple(int size) {
        int[][] table = new int[size][size];

        for (int i = 0; i < size; i++) {
            for(int j = 0; j < size; j++) {
                table[i][j] = (i + 1) * (j + 1);
            }
        }
        return table;
    }
}
