package ru.job4j.arrays;

/**
 * Task - Arrays: 6.4. The word begins with...
 * Wrapper over line.
 */
public class ArrayChar {
    private char[] data;

    public ArrayChar(String line) {
        this.data = line.toCharArray();
    }

    /**
     * Verifies that the word starts with a prefix.
     *
     * @param prefix prefix.
     * @return if the word starts with a prefix.
     */
    public boolean startWith(String prefix) {
        boolean result = true;
        char[] value = prefix.toCharArray();
        for (int i = 0; i < value.length; i++) {
            if (data[i] != value[i]) {
                result = false;
                break;
            }
        }
        return result;
    }
}