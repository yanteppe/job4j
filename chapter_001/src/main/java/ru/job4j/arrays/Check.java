package ru.job4j.arrays;

import java.util.Arrays;

/**
 * Task - Arrays: 6.3. The array is filled with true or false
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 04.01.2019
 */
public class Check {
    public boolean monoSolution1(boolean[] data) {
        boolean result = true;
        for (int i = 0; i < data.length; i++) {
            if (data[i] != data[0]) {
                result = false;
                break;
            }
        }
        System.out.println(result);
        return result;
    }

    // Second solution
    public boolean monoSolution2(boolean[] data) {
        boolean result = false;
        for (boolean i : data) {
            String var = Arrays.toString(data);
            if (!(var.contains("false") && var.contains("true"))) {
                result = true;
            }
        }
        return result;
    }
}





