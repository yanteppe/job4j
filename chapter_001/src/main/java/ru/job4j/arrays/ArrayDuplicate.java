package ru.job4j.arrays;

import java.util.Arrays;

/**
 * Task Arrays: 6.8. Remove duplicates in an array.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 17.01.2019
 */
public class ArrayDuplicate {

    public String[] remove(String[] array) {
        int element = array.length;
        // цикл перебора массива
        for (int i = 0; i < element; i++) {
            // цикл для срвенения элементов - поиск дубликатов
            for (int j = i + 1; j < element; j++) {
                if (array[i].equals(array[j])) {    // сравнение элементов на дубликаты
                    array[j] = array[element - 1];  // перестановка на место дубликата последнего элемента
                    element--;
                    j--;
                }
            }
        }
        return Arrays.copyOf(array, element); // усечение лишних элементов массива через копирование элементов нужной длины
    }
}
