package ru.job4j.arrays;

/**
 * Array flip.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 01.01.2019
 */
public class Turn {

    public int[] back(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int result = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = result;
        }
        return array;
    }
}
