package com.yan.encapsulation;

import com.yan.tracker.core.Item;
import com.yan.tracker.core.Tracker;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class TrackerTest {

    @Test
    public void verifyAddNewItemInTracker() {
        Tracker tracker = new Tracker();
        long created = System.currentTimeMillis();
        Item item = new Item("Item_1", "desc_1", created);
        tracker.add(item);
        Item result = tracker.findById(item.getId());
        assertThat(result.getName(), is(item.getName()));
    }

    @Test
    public void verifyReplaceItemInTracker() {
        Tracker tracker = new Tracker();
        Item previousItem = new Item("Item_1", "desc_1", 123L);
        tracker.add(previousItem);
        Item newItem = new Item("Item_2", "desc_2", 1234L);
        newItem.setId(previousItem.getId());
        tracker.replace(previousItem.getId(), newItem);
        assertThat(tracker.findById(previousItem.getId()).getName(), is("Item_2"));
    }

    @Test
    public void verifyRemovalItemFromTracker() {
        Tracker tracker = new Tracker();
        Item item1 = new Item("Item_1", "desc_1", 1234L);
        tracker.add(item1);
        assertThat(tracker.findById(item1.getId()), is(item1));
        tracker.delete(item1.getId());
        List<Item> result = Arrays.asList(tracker.findAll());
        assertTrue(!result.contains(item1));
    }

    @Test
    public void verifyFindOfAllCreatedItems() {
        Tracker tracker = new Tracker();
        Item item1 = new Item("Item_1", "desc_1", 1234L);
        Item item2 = new Item("Item_2", "desc_2", 1234L);
        Item item3 = new Item("Item_3", "desc_3", 1234L);
        List<Item> expectedItems = Arrays.asList(tracker.add(item1), tracker.add(item2), tracker.add(item3));
        List<Item> actualItems = Arrays.asList(tracker.findAll());
        assertEquals(actualItems, expectedItems);
    }

    @Test
    public void positiveVerifyFindItemsByName() {
        Tracker tracker = new Tracker();
        Item item1 = new Item("Item_1", "desc_0", 1234L);
        Item item2 = new Item("Item_1", "desc_1", 1234L);
        Item item3 = new Item("Item_2", "desc_2", 1234L);
        tracker.add(item1);
        tracker.add(item2);
        tracker.add(item3);
        Item[] actualItem = tracker.findByName("Item_2");
        assertEquals(item3, actualItem[0]);
    }

    @Test
    public void positiveVerifyFindItemsBySameName() {
        Tracker tracker = new Tracker();
        Item item1 = new Item("Item_1", "desc_0", 1234L);
        Item item2 = new Item("Item_1", "desc_1", 1234L);
        Item item3 = new Item("Item_2", "desc_2", 1234L);
        tracker.add(item1);
        tracker.add(item2);
        tracker.add(item3);
        Item[] expectedItems = {item1, item2};
        Item[] actualItems = tracker.findByName("Item_1");
        assertArrayEquals(expectedItems, actualItems);
    }

    @Test
    public void verifyFindItemsById() {
        Tracker tracker = new Tracker();
        Item item1 = new Item("Item_1", "desc_1", 1234L);
        Item item2 = new Item("Item_2", "desc_2", 1234L);
        Item item3 = new Item("Item_3", "desc_3", 1234L);
        tracker.add(item1);
        tracker.add(item2);
        tracker.add(item3);
        Item actualItem = tracker.findById(item2.getId());
        assertEquals(actualItem, item2);
    }

    @Test
    public void verifyNotFindItemsById() {
        Tracker tracker = new Tracker();
        Item item1 = new Item("Item_1", "desc_1", 1234L);
        Item item2 = new Item("Item_2", "desc_2", 1234L);
        Item item3 = new Item("Item_3", "desc_3", 1234L);
        tracker.add(item1);
        tracker.add(item2);
        tracker.add(item3);
        Item item = tracker.findById("666");
        assertNull(item);
    }
}
