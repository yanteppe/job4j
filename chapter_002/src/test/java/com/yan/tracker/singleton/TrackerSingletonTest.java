package com.yan.tracker.singleton;

import com.yan.patterns.singleton.TrackerEagerSingleton;
import com.yan.patterns.singleton.TrackerLazySingleton;
import com.yan.tracker.core.Tracker;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TrackerSingletonTest {

    @Test
    public void verifyTrackerLazySingleton() {
        Tracker tracker1 = TrackerLazySingleton.getInstance();
        Tracker tracker2 = TrackerLazySingleton.getInstance();
        assertEquals(tracker1, tracker2);
    }

    @Test
    public void verifyTrackerEagerSingleton() {
        Tracker tracker1 = TrackerEagerSingleton.getInstance();
        Tracker tracker2 = TrackerEagerSingleton.getInstance();
        assertEquals(tracker1, tracker2);
    }
}
