package com.yan.tracker;

import com.yan.tracker.core.Input;
import com.yan.tracker.core.Item;
import com.yan.tracker.core.StubInput;
import com.yan.tracker.core.Tracker;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class StartUITest {
    private final PrintStream stdout = System.out; // поле содержит дефолтный вывод в консоль.
    private final ByteArrayOutputStream out = new ByteArrayOutputStream(); // буфер для результата.
    private static final String MENU = "Menu:\n" +
            "0. Add new Item.\n" +
            "1. Show all Items.\n" +
            "2. Edit Item.\n" +
            "3. Delete Item.\n" +
            "4. Find Item by Id.\n" +
            "5. Find Items by name.\n" +
            "6. Exit program.";

    @Before
    public void loadOutput() {
        System.setOut(new PrintStream(this.out));
    }

    @After
    public void backOutput() {
        System.setOut(this.stdout);
    }

    /**
     * Test Menu: operation 0. Add new item
     */
    @Test
    public void verifyAddNewItemInTracker() {
        Tracker tracker = new Tracker();
        Input input = new StubInput(new String[]{"0", "Item", "description", "y"});
        new StartUI(input, tracker).startProgram();
        assertThat(tracker.findAll()[0].getName(), is("Item"));
    }

    /**
     * Test Menu: operation 1. Show all items
     */
    @Test
    public void verifyReceivedAllItems() {
        Tracker tracker = new Tracker();
        Input input1 = new StubInput(new String[]{"0", "Item_1", "description_1", "y"});
        Input input2 = new StubInput(new String[]{"0", "Item_2", "description_2", "y"});
        Input input3 = new StubInput(new String[]{"0", "Item_3", "description_3", "y"});
        new StartUI(input1, tracker).startProgram();
        new StartUI(input2, tracker).startProgram();
        new StartUI(input3, tracker).startProgram();
        assertThat(tracker.findAll()[0].getName(), is("Item_1"));
        assertThat(tracker.findAll()[1].getName(), is("Item_2"));
        assertThat(tracker.findAll()[2].getName(), is("Item_3"));
    }

    /**
     * Test Menu: operation 2. Edit item
     */
    @Test
    public void verifyReplaceItemInTracker() {
        Tracker tracker = new Tracker();
        Item item = tracker.add(new Item("Test Item", "test description", 6L));
        Input input = new StubInput(new String[]{"2", item.getId(), "New Item", "replaced item", "y"});
        new StartUI(input, tracker).startProgram();
        assertThat(tracker.findById(item.getId()).getName(), is("New Item"));
    }

    /**
     * Test Menu: operation 3. Delete item
     */
    @Test
    public void verifyDeleteItem() {
        Tracker tracker = new Tracker();
        Item item = tracker.add(new Item("TestItem", "test description", 6L));
        int actualValue = tracker.findAll().length;
        String itemId = item.getId();
        Input inputDeleteItem = new StubInput(new String[]{"3", itemId, "y"});
        new StartUI(inputDeleteItem, tracker).startProgram();
        int expectedValue = tracker.findAll().length;
        assertTrue(expectedValue < actualValue);
    }

    /**
     * Test Menu: operation 4. Find item by Id
     */
    @Test
    public void verifyFindItemById() {
        // Тут наверное правильней проверять вывод Item найденого по Id.
        // Но написано это пока не делать. Оставил так.
        Tracker tracker = new Tracker();
        Item item1 = tracker.add(new Item("Item_1", "test description 1", 6L));
        Item item2 = tracker.add(new Item("Item_2", "test description 2", 6L));
        Item item3 = tracker.add(new Item("Item_3", "test description 3", 6L));
        String actualItem = item3.getId();
        Input inputFindItem = new StubInput(new String[]{"4", actualItem, "y"});
        new StartUI(inputFindItem, tracker).startProgram();
        String expectedItem = tracker.findById(actualItem).getId();
        assertEquals(actualItem, expectedItem);
    }

    /**
     * Test console output by method showAllItems()
     * Menu operation: 1. Show all items.
     */
    @Test
    public void verifyConsoleOutputShowAllItems() {
        Tracker tracker = new Tracker();
        Item item1 = tracker.add(new Item("Item1", "desc1 ", 6L));
        Item item2 = tracker.add(new Item("Item2", "desc2 ", 6L));
        Item item3 = tracker.add(new Item("Item3", "desc3 ", 6L));
        Input inputShowAllItems = new StubInput(new String[]{"1", "y"});
        new StartUI(inputShowAllItems, tracker).startProgram();
        String expectedOutput = String.format(MENU + "\n" +
                        "\n----------------- All Items ------------------\n" +
                        "Id: %s; Name: Item1; Content: desc1 \n" +
                        "Id: %s; Name: Item2; Content: desc2 \n" +
                        "Id: %s; Name: Item3; Content: desc3 \n" +
                        "---------------- Total items: " + tracker.findAll().length + " --------------\n",
                item1.getId(), item2.getId(), item3.getId());
        String actualOutput = new String(out.toByteArray());
        assertThat(actualOutput, is(new StringBuilder().append(expectedOutput).append(System.lineSeparator()).toString()));
    }

    /**
     * Test console output by method findItemById()
     * Menu operation: 4. Find item by Id.
     */
    @Test
    public void verifyConsoleOutputFindItemById() {
        Tracker tracker = new Tracker();
        Item item = tracker.add(new Item("testItem", "testDesc ", 6L));
        Input inputFindItemById = new StubInput(new String[]{"4", item.getId(), "y"});
        new StartUI(inputFindItemById, tracker).startProgram();
        String expectedOutput = String.format(MENU + "\n" +
                "\n----------------- Find Item by Id ------------------\n" +
                "Id: %s; Name: testItem; Content: testDesc \n" +
                "------------------------- End ----------------------\n", item.getId());
        String actualOutput = new String(out.toByteArray());
        assertThat(actualOutput, is(new StringBuilder().append(expectedOutput).append(System.lineSeparator()).toString()));
    }

    /**
     * Test console output by method findItemByName()
     * Menu operation: 5. Find items by name.
     */
    @Test
    public void verifyConsoleOutputFindItemByName() {
        Tracker tracker = new Tracker();
        Item item = tracker.add(new Item("testItem", "testDesc ", 6L));
        Input inputFindItemByName = new StubInput(new String[]{"5", "testItem", "y"});
        new StartUI(inputFindItemByName, tracker).startProgram();
        String expectedOutput = String.format(MENU + "\n" +
                "\n----------------- Find Item by name ------------------\n" +
                "Id: %s; Name: testItem; Content: testDesc \n" +
                "------------------------- End ----------------------\n", item.getId());
        String actualOutput = new String(out.toByteArray());
        assertThat(actualOutput, is(new StringBuilder().append(expectedOutput).append(System.lineSeparator()).toString()));
    }

    /**
     * Test Menu: operation 5. Find items by name
     */
    @Test
    public void verifyFindItemsByName() {
        // Пока не нужно реализовывать
    }
}
