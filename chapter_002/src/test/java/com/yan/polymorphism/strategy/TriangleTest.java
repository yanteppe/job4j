package com.yan.polymorphism.strategy;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Test class Triangle
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @see Triangle
 * @since 06.04.2019
 */
public class TriangleTest {

    @Test
    public void verifyDrawIsTriangle() {
        Triangle triangle = new Triangle();
        assertThat(triangle.draw(), is(
                new StringBuilder()
                        .append("   +    ")
                        .append("  +  +  ")
                        .append(" +    + ")
                        .append("++++++++")
                        .toString()));
    }
}
