package com.yan.polymorphism.strategy;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * @author Yan Teppe (yanteppe@gmail.com)
 */
public class PaintTest {
    private final PrintStream stdout = System.out; // поле содержит дефолтный вывод в консоль.
    private final ByteArrayOutputStream out = new ByteArrayOutputStream(); // буфер для результата.

    @Before
    public void loadOutput() {
        System.out.println("execute before method");
        System.setOut(new PrintStream(this.out));
    }

    @After
    public void backOutput() {
        System.setOut(this.stdout);
        System.out.println("execute after method");
    }

    @Test
    public void verifyDrawIsSquare() {
        new Paint().draw(new Square());
        // Проверяем результат вычисления
        assertThat(new String(out.toByteArray()), is(
                new StringBuilder()
                        .append("++++")
                        .append("+  +")
                        .append("+  +")
                        .append("++++")
                        .append(System.lineSeparator())
                        .toString()));
        this.backOutput(); // Возвращаем обратно стандартный вывод в консоль.
    }

    @Test
    public void verifyDrawIsTriangle() {
        new Paint().draw(new Triangle());
        assertThat(new String(out.toByteArray()), is(
                new StringBuilder()
                        .append("   +    ")
                        .append("  +  +  ")
                        .append(" +    + ")
                        .append("++++++++")
                        .append(System.lineSeparator())
                        .toString()));
        this.backOutput(); // Возвращаем обратно стандартный вывод в консоль.
    }
}

