package com.yan.polymorphism.strategy;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Test class Square
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 * @see Square
 * @since 06.04.2019
 */
public class SquareTest {

    @Test
    public void verifyDrawIsSquare() {
        Square square = new Square();
        assertThat(square.draw(), is(
                new StringBuilder()
                        .append("++++")
                        .append("+  +")
                        .append("+  +")
                        .append("++++")
                        .toString()));
    }
}
