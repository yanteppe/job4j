package com.yan.inheritance;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class EngineerTest {

    @Test
    public void engineerShouldBeHaveName(){
        Engineer engineer = new Engineer();
        engineer.setName("Вася");
        String expectedName = "Вася";
        String actualName = engineer.getName();
        assertThat(actualName, is(expectedName));
    }

    @Test
    public void engineerShouldBeBuildingHome(){
        Engineer engineer = new Engineer();
        Home home = new Home();
        home.setAddress("Chicago");
        home.setFloor(2);
        Home actualHome = engineer.build(home);
        assertThat(actualHome, is(home));
    }
}
