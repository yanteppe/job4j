package com.yan.inheritance;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class DoctorTest {

    @Test
    public void doctorShouldBeHaveName(){
        Doctor doctor = new Doctor();
        doctor.setName("Айболит");
        String expectedName = "Айболит";
        String actualName = doctor.getName();
        assertThat(actualName, is(expectedName));
    }

    @Test
    public void doctorShouldBeHealPatient(){
        Doctor doctor = new Doctor();
        Patient patient = new Patient();
        patient.setDiagnose(new Diagnose("Ветрянка"));
        Diagnose actualDiagnose = patient.getDiagnose();
        Diagnose expectedDiagnose = doctor.heal(patient);
        assertThat(actualDiagnose, is(expectedDiagnose));
    }
}
