package com.yan.inheritance;

public class Doctor extends Profession {

    public Diagnose heal(Patient patient){
        return patient.getDiagnose();
    }
}
