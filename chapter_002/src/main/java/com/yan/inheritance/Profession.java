package com.yan.inheritance;

public class Profession {
    private String name;
    private String profession;

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
