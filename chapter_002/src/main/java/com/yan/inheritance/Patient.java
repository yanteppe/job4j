package com.yan.inheritance;

public class Patient {
    private String name;
    private Diagnose diagnose;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDiagnose(Diagnose diagnose) {
        this.diagnose = diagnose;
    }

    public Diagnose getDiagnose() {
        return diagnose;
    }
}
