package com.yan.patterns.singleton;

import com.yan.tracker.core.Tracker;

/**
 * static final field. Eager loading.
 */
public class TrackerEagerSingleton {
    private static final Tracker INSTANCE = new Tracker();

    private TrackerEagerSingleton() {}

    public static Tracker getInstance() {
        return INSTANCE;
    }
}
