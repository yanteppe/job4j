package com.yan.patterns.singleton;

import com.yan.tracker.core.Tracker;

/**
 * Patterns - singleton
 * Ленивая загрузка (Lazy loading) происходит, когда мы явно обращаемся к объекту. Происходит его загрузка.
 * static field. Lazy loading.
 */
public class TrackerLazySingleton {
    private static Tracker instance;

    private TrackerLazySingleton() {}

    public static Tracker getInstance() {
        if (instance == null) {
            instance = new Tracker();
        }
        return instance;
    }
}
