package com.yan.polymorphism.strategy;

/**
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 04.04.2019
 */
public class Triangle implements Shape {

    public String draw() {
        StringBuilder pic = new StringBuilder();
        pic.append("   +    ");
        pic.append("  +  +  ");
        pic.append(" +    + ");
        pic.append("++++++++");
        return pic.toString();
    }
}
