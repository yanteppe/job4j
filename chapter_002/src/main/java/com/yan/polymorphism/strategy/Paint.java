package com.yan.polymorphism.strategy;

/**
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 04.04.2019
 */
public class Paint{
    public Shape shape;

    public void draw(Shape shape){
        System.out.println(shape.draw());;
    }
}
