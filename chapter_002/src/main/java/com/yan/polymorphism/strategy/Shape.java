package com.yan.polymorphism.strategy;

/**
 * @author Yan Teppe (yanteppe@gmail.com)
 * @since 04.04.2019
 */
public interface Shape {

    String draw();
}
