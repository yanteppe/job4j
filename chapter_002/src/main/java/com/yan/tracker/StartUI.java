package com.yan.tracker;

import com.yan.tracker.core.Input;
import com.yan.tracker.core.MenuTracker;
import com.yan.tracker.core.Tracker;
import com.yan.tracker.core.ValidateInput;

public class StartUI {
    private final Input input;          // Receive data from the user.
    private final Tracker tracker;      // Repository of item.
    private boolean runProgram = true;  // State of program

    /**
     * Field initialization constructor.
     *
     * @param input   data input.
     * @param tracker repository of item.
     */
    public StartUI(Input input, Tracker tracker) {
        this.input = input;
        this.tracker = tracker;
    }

    /**
     * Start main program cycle.
     */
    public void startProgram() {
        MenuTracker menu = new MenuTracker(this.input, this.tracker, this);
        menu.fillActions();
        int[] range = new int[menu.getActionsLength()];
        for (int i = 0; i < range.length; i++) {
            range[i] = i;
        }
        do {
            menu.showMenu();
            menu.select(input.ask("Select: ", range));
            if (!runProgram) break;
        } while (!"y".equals(this.input.ask("Exit? (y/n): ")));
    }

    /**
     * Stop program
     */
    public void stopProgram() {
        this.runProgram = false;
    }

    /**
     * Start program.
     *
     * @param args
     */
    public static void main(String[] args) {
        new StartUI(new ValidateInput(), new Tracker()).startProgram();
    }
}
