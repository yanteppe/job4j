package com.yan.tracker.core;

/**
 * Class for handling the wrong menu selection
 *
 */
public class MenuOutException extends RuntimeException {

    public MenuOutException(String message) {
        super(message);
    }
}
