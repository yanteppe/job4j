package com.yan.tracker.core;

/**
 * Interface defining behavior of User input in program.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 */
public interface Input {

    String ask(String question);

    int ask(String question, int[] range);
}
