package com.yan.tracker.core.actions;

import com.yan.tracker.core.BaseAction;
import com.yan.tracker.core.Input;
import com.yan.tracker.core.Item;
import com.yan.tracker.core.Tracker;

public class FindItemById extends BaseAction {

    public FindItemById(int key, String name) {
        super(key, name);
    }

    @Override
    public void execute(Input input, Tracker tracker) {
        System.out.println();
        System.out.println("----------------- Find Item by Id ------------------");
        String itemId = input.ask("Enter item id for searching: ");
        Item foundItem = tracker.findById(itemId);
        if (foundItem == null) {
            System.out.println("Item not found");
        } else {
            System.out.printf("Id: %s; Name: %s; Content: %s", foundItem.getId(), foundItem.getName(), foundItem.getDescription()).println();
            System.out.println("------------------------- End ----------------------");
            System.out.println();
        }
    }
}
