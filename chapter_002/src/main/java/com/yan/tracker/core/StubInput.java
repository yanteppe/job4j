package com.yan.tracker.core;

/**
 * Class stub for program testing.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 */
public class StubInput implements Input {
    private final String[] value;
    private int position = 0;

    public StubInput(String[] value) {
        this.value = value;
    }

    public String ask(String question) {
        return this.value[position++];
    }

    @Override
    public int ask(String question, int[] range) {
        int key = Integer.valueOf(this.ask(question));
        boolean result = false;
        for (int value : range) {
            if (value == key) {
                result = true;
                break;
            }
        }
        if (result) return key;
        else throw new MenuOutException("Selected is wrong menu item, please select from 0 to 6");
    }
}