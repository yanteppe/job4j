package com.yan.tracker.core;

import java.util.Arrays;
import java.util.Date;
import java.util.Random;

public class Tracker {
    private final Item[] items = new Item[100];
    private int position = 0;

    public Item add(Item item) {
        item.setId(generateId());
        this.items[this.position++] = item;
        return item;
    }

    public boolean delete(String id) {
        boolean result = false;
        Item deletedItem = findById(id);
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i].equals(deletedItem)) {
                this.items[i] = null;
                int deletedIndex = i + 1;
                System.arraycopy(this.items, deletedIndex, this.items, i, this.items.length - deletedIndex);
                result = true;
                break;
            }
        }
        return result;
    }

    public Item[] findAll() {
        int countItems = 0;
        Item[] resultItems = new Item[0];
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i] == null) {
                countItems = i;
                resultItems = new Item[countItems];
                for (int j = 0; j < resultItems.length; j++) resultItems[j] = this.items[j];
                break;
            }
        }
        return resultItems;
    }

    public Item[] findByName(String key) {
        Item[] arrayItems = new Item[this.items.length];
        int countItems = 0;
        for (Item item : this.items) {
            if (item == null) break;
            else if (item.getName().equals(key)) {
                arrayItems[countItems] = item;
                countItems++;
            }
        }
        return Arrays.copyOf(arrayItems, countItems);
    }

    public Item findById(String id) {
        Item result = null;
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i] == null) break;
            if (items[i].getId().equals(id)) {
                result = items[i];
                break;
            }
        }
        return result;
    }

    private String generateId() {
        Date date = new Date();
        Random random = new Random();
        short currentTime = (short) date.getTime();
        String uniqueId = String.valueOf(random.nextInt(Math.abs(currentTime)));
        return uniqueId;
    }

    public boolean replace(String id, Item newItem) {
        for (int i = 0; i < items.length; i++) {
            if (items[i].getId().equals(id)) {
                items[i] = newItem;
                newItem.setId(id);
                return true;
            }
        }
        return false;
    }
}
