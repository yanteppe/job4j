package com.yan.tracker.core.actions;

import com.yan.tracker.core.BaseAction;
import com.yan.tracker.core.Item;
import com.yan.tracker.core.Tracker;
import com.yan.tracker.core.Input;

public class EditItem extends BaseAction {

    public EditItem(int key, String name) {
        super(key, name);
    }

    @Override
    public void execute(Input input, Tracker tracker) {
        System.out.println("------------ Edit item --------------");
        String itemId = input.ask("Enter item id for editing: ");
        String newItemName = input.ask("Enter new item name: ");
        String desc = input.ask("Enter item description: ");
        Item newItem = new Item(newItemName, desc, 1234L);
        String result = (tracker.replace(itemId, newItem)) ? ("SUCCESSFULLY") : ("FAILED");
        System.out.println("------------ Operation result: " + result + " -----------");
    }
}
