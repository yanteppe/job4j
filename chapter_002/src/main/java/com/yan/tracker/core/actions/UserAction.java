package com.yan.tracker.core.actions;

import com.yan.tracker.core.Input;
import com.yan.tracker.core.Tracker;

/**
 * Interface defining user action
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 */
public interface UserAction {

    /**
     * Returns the user action key by menu
     *
     * @return menu action key
     */
    int menuActionKey();

    /**
     * Main method for executing program actions
     *
     * @param input   object for input data in program
     * @param tracker object for working of Items
     */
    void execute(Input input, Tracker tracker);

    /**
     * Method returns information about selected menu
     *
     * @return selected menu string
     */
    String info();
}
