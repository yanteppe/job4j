package com.yan.tracker.core.actions;

import com.yan.tracker.StartUI;
import com.yan.tracker.core.BaseAction;
import com.yan.tracker.core.Input;
import com.yan.tracker.core.Tracker;

public class ExitProgram extends BaseAction {
    private final StartUI startUI;

    public ExitProgram(int key, String name, StartUI startUI) {
        super(key, name);
        this.startUI = startUI;
    }

    @Override
    public void execute(Input input, Tracker tracker) {
        System.out.println("Program is over");
        this.startUI.stopProgram();
    }
}
