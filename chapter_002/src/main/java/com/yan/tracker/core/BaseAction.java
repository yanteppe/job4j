package com.yan.tracker.core;

import com.yan.tracker.core.actions.UserAction;

/**
 * Class for base actions
 */
public abstract class BaseAction implements UserAction {
    private final int key;
    private final String name;

    public BaseAction(final int key, final String name) {
        this.key = key;
        this.name = name;
    }

    @Override
    public int menuActionKey() {
        return this.key;
    }

    @Override
    public String info() {
        return String.format("%s. %s", this.key, this.name);
    }
}
