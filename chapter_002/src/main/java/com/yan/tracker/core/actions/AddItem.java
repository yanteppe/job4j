package com.yan.tracker.core.actions;

import com.yan.tracker.core.BaseAction;
import com.yan.tracker.core.Input;
import com.yan.tracker.core.Item;
import com.yan.tracker.core.Tracker;

public class AddItem extends BaseAction {

    public AddItem(int key, String name) {
        super(key, name);
    }

    @Override
    public void execute(Input input, Tracker tracker) {
        System.out.println("------------ Adding a new item --------------");
        String name = input.ask("Enter item name: ");
        String desc = input.ask("Enter item description: ");
        Item item = new Item(name, desc, 1234L);
        tracker.add(item);
        System.out.println("------------ New item with id : " + item.getId() + " -----------");
    }
}
