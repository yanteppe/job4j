package com.yan.tracker.core.actions;

import com.yan.tracker.core.BaseAction;
import com.yan.tracker.core.Input;
import com.yan.tracker.core.Tracker;

public class DeleteItem extends BaseAction {

    public DeleteItem(int key, String name) {
        super(key, name);
    }

    @Override
    public void execute(Input input, Tracker tracker) {
        System.out.println();
        System.out.println("----------------- Deleting Item ------------------");
        String itemId = input.ask("Enter item id for deleting: ");
        String result = (tracker.delete(itemId)) ? ("SUCCESSFULLY") : ("FAILED");
        System.out.println("------------ Operation result: " + result + " -----------");
    }
}
