package com.yan.tracker.core;

import java.util.Scanner;

/**
 * Class implements user input in program.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 */
public class ConsoleInput implements Input {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public String ask(String question) {
        System.out.println(question);
        return scanner.nextLine();
    }

    @Override
    public int ask(String question, int[] range) {
        int key = Integer.valueOf(this.ask(question));
        boolean result = false;
        for (int value : range) {
            if (value == key) {
                result = true;
                break;
            }
        }
        if (result) return key;
        else throw new MenuOutException("Selected is wrong menu item, please select from 0 to 6");
    }
}
