package com.yan.tracker.core;

/**
 * Class to validate user input.
 *
 * @author Yan Teppe (yanteppe@gmail.com)
 */
public class ValidateInput extends ConsoleInput {

    public int ask(String question, int[] range) {
        boolean result = true;
        int value = -1;
        do {
            try {
                value = super.ask(question, range);
                result = false;
            } catch (MenuOutException menuOutException) {
                System.out.println("Selected invalid item of menu, please select item of menu from 0 to 6");
            } catch (NumberFormatException numberFormatException) {
                System.out.println("Invalid data entered, please enter again");
            }
        } while (result);
        return value;
    }
}
