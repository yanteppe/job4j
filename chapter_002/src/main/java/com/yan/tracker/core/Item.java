package com.yan.tracker.core;

import java.util.Arrays;
import java.util.Objects;

public class Item {
    private String name;
    private String description;
    private long creationTime;
    private int[] comments;
    private String id;

    public Item(String name, String description, long time) {
        this.name = name;
        this.description = description;
        this.creationTime = time;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public int[] getComments() {
        return comments;
    }

    public void setComments(int[] comments) {
        this.comments = comments;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) return true;
        if (!(object instanceof Item)) return false;
        Item item = (Item) object;
        return creationTime == item.creationTime &&
                Objects.equals(name, item.name) &&
                Objects.equals(description, item.description) &&
                Arrays.equals(comments, item.comments) &&
                Objects.equals(id, item.id);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, description, creationTime, id);
        result = 31 * result + Arrays.hashCode(comments);
        return result;
    }
}
