package com.yan.tracker.core;

import com.yan.tracker.StartUI;
import com.yan.tracker.core.actions.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Class implements menu work in program.
 */
public class MenuTracker {
    private Input input;
    private Tracker tracker;
    private List<UserAction> actions = new ArrayList<>();
    private final StartUI startUI;

    /**
     * Constructor of MenuTracker.
     *
     * @param input   object type Input
     * @param tracker object type Tracker
     */
    public MenuTracker(Input input, Tracker tracker, StartUI startUI) {
        this.input = input;
        this.tracker = tracker;
        this.startUI = startUI;
    }

    /**
     * Getter for getting menu array
     *
     * @return arrays length
     */
    public int getActionsLength() {
        return this.actions.size();
    }

    /**
     * Fill list of menu actions.
     */
    public void fillActions() {
        this.actions.add(new AddItem(0, "Add new Item."));
        this.actions.add(new ShowItems(1, "Show all Items."));
        this.actions.add(new EditItem(2, "Edit Item."));
        this.actions.add(new DeleteItem(3, "Delete Item."));
        this.actions.add(new FindItemById(4, "Find Item by Id."));
        this.actions.add(new FindItemsByName(5, "Find Items by name."));
        this.actions.add(new ExitProgram(6, "Exit program.", startUI));
    }

    /**
     * Execute action by key.
     *
     * @param key menu operation key
     */
    public void select(int key) {
        this.actions.get(key).execute(this.input, this.tracker);
    }

    /**
     * Display menu.
     */
    public void showMenu() {
        System.out.println("Menu:");
        for (UserAction action : this.actions) {
            if (action != null) {
                System.out.println(action.info());
            }
        }
    }
}
