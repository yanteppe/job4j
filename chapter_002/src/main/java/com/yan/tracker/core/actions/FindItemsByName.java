package com.yan.tracker.core.actions;

import com.yan.tracker.core.BaseAction;
import com.yan.tracker.core.Input;
import com.yan.tracker.core.Tracker;

import java.util.Arrays;

public class FindItemsByName extends BaseAction {

    public FindItemsByName(int key, String name) {
        super(key, name);
    }

    @Override
    public void execute(Input input, Tracker tracker) {
        System.out.println();
        System.out.println("----------------- Find Item by name ------------------");
        String nameItem = input.ask("Enter item name for searching: ");
        Arrays.stream(tracker.findByName(nameItem))
                .forEach(item -> System.out.printf("Id: %s; Name: %s; Content: %s", item.getId(), item.getName(), item.getDescription()).println());
        System.out.println("------------------------- End ----------------------");
        System.out.println();
    }
}
