package com.yan.tracker.core.actions;

import com.yan.tracker.core.BaseAction;
import com.yan.tracker.core.Input;
import com.yan.tracker.core.Tracker;

import java.util.Arrays;

public class ShowItems extends BaseAction {

    public ShowItems(int key, String name) {
        super(key, name);
    }

    @Override
    public void execute(Input input, Tracker tracker) {
        System.out.println();
        System.out.println("----------------- All Items ------------------");
        Arrays.stream(tracker.findAll())
                .forEach(item -> System.out.printf("Id: %s; Name: %s; Content: %s", item.getId(), item.getName(), item.getDescription()).println());
        System.out.println("---------------- Total items: " + tracker.findAll().length + " --------------");
        System.out.println();
    }
}
