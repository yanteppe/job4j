package sandbox.factory_method;

public class PassengerCarMaker implements CarFactory {

    @Override
    public Car createCar() {
        return new PassengerCar();
    }
}
