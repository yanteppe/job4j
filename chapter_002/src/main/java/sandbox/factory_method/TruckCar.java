package sandbox.factory_method;

public class TruckCar implements Car {

    @Override
    public void carType() {
        System.out.println("Car: truck");
    }
}
