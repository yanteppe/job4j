package sandbox.factory_method;

public class PassengerCar implements Car {

    @Override
    public void carType() {
        System.out.println("Car: passenger car");
    }
}
