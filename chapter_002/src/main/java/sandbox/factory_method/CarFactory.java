package sandbox.factory_method;

interface CarFactory {
    // Фабричный метод
    Car createCar();
}
