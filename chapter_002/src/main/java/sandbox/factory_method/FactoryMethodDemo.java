package sandbox.factory_method;

/**
 * Шаблон фабричный метод - использование
 */
public class FactoryMethodDemo {

    public static void main(String[] args) {

        CarFactory carFactory = new PassengerCarMaker();
        Car car = carFactory.createCar();
        car.carType();
        System.out.println("---");

        CarFactory carFactory1 = new FactoryMethodDemo().createCarByType(CarType.TRUCK);
        carFactory1.createCar().carType();
    }

    /**
     * Тип автомобиля
     */
    enum CarType {
        PASSENGER("passenger"),
        TRUCK("truck");

        private String carType;

        CarType(String carType) {
            this.carType = carType;
        }

        public String getCarType() {
            return carType;
        }
    }

    /**
     * Создать автомобиль по типу транспортного средства
     *
     * @return cars
     */
    public CarFactory createCarByType(CarType carType) {
        if (carType.equals(CarType.PASSENGER))
            return new PassengerCarMaker();
        else if (carType.equals(CarType.TRUCK))
            return new TruckCarMaker();

        throw new RuntimeException("Invalid car type: " + carType);
    }
}
