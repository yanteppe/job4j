package sandbox.factory_method;

public class TruckCarMaker implements CarFactory {

    @Override
    public Car createCar() {
        return new TruckCar();
    }
}
